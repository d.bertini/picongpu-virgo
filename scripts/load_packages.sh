#
# Utility script for loading picongpu soft. stack
# needed for project compilation.
#


echo "loading spack packages ..."
spack load openmpi%gcc arch=x86_64
spack load gcc
spack load hdf5%gcc arch=x86_64
spack load cmake%gcc arch=x86_64
spack load libpng%gcc arch=x86_64
spack load hip%gcc arch=x86_64

echo " done!."

# Main environment variables
export PICHOME=/lustre/rz/dbertini/gpu/picongpu
export PICSRC=$PICHOME
export PIC_EXAMPLES=$PICSRC/share/picongpu/examples
export PATH=$PICSRC:$PATH
export PATH=$PICSRC/bin:$PATH
export PATH=$PICSRC/src/tools/bin:$PATH
export PYTHONPATH=$PICSRC/lib/python:$PYTHONPATH

# Dependencies
export SOFTW_PATH=/lustre/rz/dbertini/gpu/softw/picongpu_softw
export ADIOS2_DIR=$SOFTW_PATH/adios2
export openPMD_DIR=$SOFTW_PATH/openpmd-api
export PNGwriter_DIR=$SOFTW_PATH/pngwriter_0.7.0
export rocrand_DIR=$SOFTW_PATH/rocrand/rocrand/lib/cmake
#export ISAAC_DIR=$SOFTW_PATH/isaac/

# Boost includes
export CPLUS_INCLUDE_PATH=$SOFTW_PATH/boost_1.75_0/include:$CPLUS_INCLUDE_PATH

## add hip+adios
export LD_LIBRARY_PATH=$SOFTW_PATH/rocrand/hiprand/lib:$SOFTW_PATH/rocrand/rocrand/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$ADIOS2_DIR/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$openPMD_DIR/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PNGwriter_DIR/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$SOFTW_PATH/boost_1.75_0/lib:$LD_LIBRARY_PATH


# hip executables
export PATH=$SOFTW_PATH/rocrand/hiprand/bin:$PATH
export PATH=$ADIOS2_DIR/bin:$PATH

# output data
export SCRATCH=/lustre/rz/dbertini/gpu/vae_tests/data
export WORKDIR=/lustre/rz/dbertini/gpu/vae_tests

## MPI
export OMPI_MCA_io=^ompio
export OMPI_MCA_mpi_leave_pinned=0
export OMPI_MCA_btl_openib_allow_ib=1
export OMPI_MCA_btl_openib_rdma_pipeline_send_length=100000000
export OMPI_MCA_btl_openib_rdma_pipeline_frag_size=100000000


echo " check paths ..."
echo "LD_LIBRARY_PATH"
echo $LD_LIBRARY_PATH

echo "PATH"
echo $PATH

echo " check compilers..."
export CC=`which clang`
export CXX=`which hipcc`
echo $CC
echo $CXX
