# picongpu-virgo

This project is intended to ease the usage of picongpu on the virgo AMD-GPU cluster.  It contains
-  a definition file for dedicated singluarity container  for the picongpu software stack.  
-  add-on TBG script template to run efficiently on virgo

<figure>
<img src="/picts/e_yx.png" alt="results"/>
    <figcaption>Transverse Energy for Electrons abtained in LWFA simulation. Simulation done with 128 AMD MI 100 GPUs  using the new virgo GPU cluster</figcaption>
</figure>



## Picongpu installation
You need first to download the `dev` version of [picongpu](https://github.com/ComputationalRadiationPhysics/picongpu).
Highly recommended is to read the [picongpu official documentation](https://picongpu.readthedocs.io/en/0.6.0/index.html).

Here we will follow a step by step approch which will bring you straight to the point of using `picongpu` on the virgo cluster.

```
git clone https://github.com/ComputationalRadiationPhysics/picongpu.git 
git checkout dev

```

After that you need to add the modified submit scripts and parameter files from this git repository to the 
downloaded picongpu source code.

```
cp -R  picongpu-virgo/picongpu/etc/picongpu/virgo-gsi picongpu/etc/picongpu/
cp  picongpu-virgo/picongpu/include/picongpu/param/memory.param picongpu/include/picongpu/param/
```

Create a working directory (on `/lustre`) for example:

```
mkdir /lustre/<group>/<user_name>/my_work_dir 

```
Add the `picongpu.profile.example` to your working directory:

```
cp picongpu/etc/picongpu/virgo-gsi/virgo_picongpu.profile.example /lustre/<group>/<user_name>/my_work_dir/picongpu.profile 

```
Adapt the paths in the copied `picongpu.profile` to your case.
Basically you will need to adapt the environment variable `$PICHOME`  at `Line 47` of the
`picongpu.profile` :

```
export PICHOME=<picongpu_full_path>

```

and the path to your working directory as well as the data output directory
at `Line 55-57`

```
# output data (use Lustre here)
export SCRATCH=<data_directory>
export WORKDIR=<working_directory>

```

Then adapt again the same variables for the template file in your `picongpu` source code

```
picongpu/etc/picongpu/virgo-gsi/virgo.tpl

```
Be careful of multiple occurences:

- `Line 178` and `Lines 187-188`
- `Line 200` and `Lines 215-216`

After all these unpleasant settings you should be able to use `picongpu` as it is.



## Getting started with `picongpu@virgo`

We will now compile and run a LWFA simulation example.
For this we need first to login to the non-containerized virgo submit node `virgo.hpc.gsi.de`:

```
ssh user_name@virgo.hpc.gsi.de
cd /lustre/<group>/<user_name>/my_working_dir

```

You have the possibility to compile and run `picongpu` using different containerized environments: 

- stand-alone container (OS: Ubuntu 24-04 LTS)
- HPC-VAE container (OS: Centos7 )

both having `ROCm 4.3.1` software stack pre-installed.

### Stand-alone container (Ubuntu)

Launch the stand-alone Ubuntu singularity container which contains all necessary system+software
stack in order to use `picongpu`.

```
export VAECONT=/cvmfs/phelix.gsi.de/sifs/ubuntu-24.04_rocm-4.3.1_picongpu.sif

singularity exec --bind /cvmfs --rocm $VAECONT bash -l  

```

#### Set the environment for compilation on Ubuntu

Set all environment variables i.e paths to all software stack for `picongpu` using the profile script from
this repository

```
cp picongpu-virgo/picongpu/etc/picongpu/virgo-gsi-stanalone/virgo_picongpu_profile.example picongpu.profile

. ./picongou.profile

```
After that, the environment is set and you can for example check the version of the `HIP` compiler `hipcc`

```
Singularity> hipcc --version
HIP version: 4.3.21331-94fc2572
clang version 13.0.0 (https://github.com/RadeonOpenCompute/llvm-project roc-4.3.1 21313 286c48af238c2d3a24ebc5a06ea5191f333eaed0)
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /opt/rocm-4.3.1/llvm/bin

```

Create the Laser Wakefield simulation example

```
cd $PICSRC
pic-create $PIC_EXAMPLES/LaserWakefield /lustre/<group>/<user_name>/my_working_dir/myLWFA

```
Build the example:

```
cd /lustre/<group>/<user_name>/my_working_dir/myLWFA
pic-build -b hip:gfx908 -c '-DBOOST_ROOT=/opt/boost/1.75.0 -DMPI_CXX_WORKS=ON -DMPI_CXX_VERSION=0'

```


### HPC-VAE container

Launch the standard HPC-VAE singularity container which contains all necessary system+software
stack in order to use `picongpu`.

```
export VAECONT=/cvmfs/vae.gsi.de/amdgpu/containers/user_container-develop.sif

singularity exec  --bind /etc/slurm --bind /var/run/munge --bind /var/spool/slurm   --bind /var/lib/sss/pipes --bind /cvmfs --rocm $VAECONT bash -l  

```
For convenience, you can execute your `bash` profile to change the singularity prompt to your
normal bash prompt, issueing for example:

```
. ~/.bash_profile
```

#### Set your environment for compilation on VAE
Once you are in the HPC container environment, use the script `load_packages.sh` from this repository to load all necessary packages via `spack`.

```
. scripts/load_packages.sh

```

Create the Laser Wakefield simulation example

```
cd $PICSRC
pic-create $PIC_EXAMPLES/LaserWakefield /lustre/<group>/<user_name>/my_working_dir/myLWFA

```
Build the example:

```
cd /lustre/<group>/<user_name>/my_working_dir/myLWFA
pic-build -b hip:gfx908 -c '-DBOOST_ROOT=$SOFTW_PATH/boost_1.75_0 -DMPI_CXX_WORKS=ON -DMPI_CXX_VERSION=0'

```


### Submitting on the Virgo `gpu` partition

Once your code is compiled, you can exit the container shell simply by issueing an explicit `exit` command.
From the virgo submit node make sure your environment variables are set, by executing
again the profile script correponding to the containerized environment you were using for compilation.

from the working directory:

```
. ./picongpu.profile

```
Now you can create and run a LWFA simulation batch job on the virgo GPU cluster using
[tbg](https://picongpu.readthedocs.io/en/0.6.0/usage/tbg.html)


#### 1.cfg file
for ADIOS I/O, once should adapt the memory buffer size properly. An example of such configuration
can be found in this repository:

```
picongpu-virgo/picongpu/etc/picongpu/1.cfg
```
Just copy the configuration to your project build repository and adapt it to run on N GPUs by changing the
devices definition for X,Y,Z directions:

For example, here is the defintion 2D simulation running with 4 GPUs in (X,Y) direction using in total 16 GPUs
corresponding to  2 Nodes on the `gpu` partition

```
TBG_devices_x=4
TBG_devices_y=4
TBG_devices_z=1

```
The submit the job using the `tbg` utility: 

```
cd /lustre/<group>/<user_name>/my_working_dir/myLWFA
tbg -s sbatch -c etc/picongpu/1.cfg -t etc/picongpu/virgo-gsi/virgo.tpl $SCRATCH/lwfa_001

```
`$SCRATCH` is an environment variable that you have defined in your profile.
The generated simulation directory `$SCRATCH/lwfa_001` will have such a scructure:

```

├── input
│   ├── bin
│   ├── cmakeFlags
│   ├── cmakeFlagsSetup
│   ├── etc
│   ├── include
│   └── lib
├── pog_36500931.err
├── pog_36500931.out
├── simOutput
│   ├── checkpoints
│   ├── e_energyHistogram_all.dat
│   ├── e_macroParticlesCount.dat
│   ├── openPMD
│   ├── output -> ../stdout
│   ├── phaseSpace
│   └── pngElectronsYX
└── tbg
    ├── cpuNumaStarter.sh
    ├── cuda.filter
    ├── handleSlurmSignals.sh
    ├── openib.conf
    ├── pic_sub.sh
    ├── scorep.filter
    ├── submitAction.sh
    ├── submit.cfg
    ├── submit.start
    └── submit.tpl

```

- `input` : copy if the input `param` files used to compiled your specific simulation
- `checkpoints` : directory containing checkpoints data for an possible restart of the simulation later on.
- `simOutput` :directory containing the simulated output data.
- `tbg`: actual scripts used to send a simulation to the cluster.
- `pog_*.err` and `pog_*.out` are the  std errors and output from your job.

When the simulation is running , you can control your job using the standard SLURM commands.


