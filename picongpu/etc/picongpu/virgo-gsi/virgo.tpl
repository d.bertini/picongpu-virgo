#!/bin/bash
# Copyright 2013-2021 Axel Huebl, Richard Pausch, Rene Widera, Sergei Bastrakov, Klaus Steinger
#
# This file is part of PIConGPU.
#
# PIConGPU is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PIConGPU is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PIConGPU.
# If not, see <http://www.gnu.org/licenses/>.
#

# PIConGPU batch script for spock's SLURM batch system


#SBATCH --partition=!TBG_queue
#SBATCH --time=!TBG_wallTime
#SBATCH --job-name=!TBG_jobName
#SBATCH --nodes=!TBG_nodes                           # Nb of nodes
#SBATCH --ntasks=!TBG_tasks                          # Nb of MPI tasks
#SBATCH --gres=gpu:!TBG_devicesPerNode
#SBATCH --cpus-per-task=!TBG_coresPerGPU           # CPU Cores per MPI process
#SBATCH --mem=0                                   # Requested Total Job Memory / Node  
#SBATCH --mem-per-gpu=!TBG_memPerDevice
#SBATCH --gpu-bind=closest 
#SBATCH --mail-type=!TBG_mailSettings
#SBATCH --mail-user=!TBG_mailAddress
#SBATCH --chdir=!TBG_dstPath

#SBATCH -o pog_%j.out
#SBATCH -e pog_%j.err

# #SBATCH --ntasks-per-node=!TBG_devicesPerNode        
# #SBATCH --mincpus=!TBG_mpiTasksPerNode


# #SBATCH --account=!TBG_nameProject
# #SBATCH --partition=!TBG_queue
# #SBATCH --time=!TBG_wallTime
# Sets batch job's name
# #SBATCH --job-name=!TBG_jobName
# #SBATCH --nodes=!TBG_nodes
# #SBATCH --ntasks=!TBG_tasks
# #SBATCH --ntasks-per-node=!TBG_devicesPerNode
# #SBATCH --mincpus=!TBG_mpiTasksPerNode
# #SBATCH --cpus-per-task=!TBG_coresPerGPU
# #SBATCH --mem-per-gpu=!TBG_memPerDevice
# # #SBATCH --gpus-per-task=1
# #SBATCH --gpu-bind=closest
# #SBATCH --mail-type=!TBG_mailSettings
# #SBATCH --mail-user=!TBG_mailAddress
# #SBATCH --chdir=!TBG_dstPath



## calculations will be performed by tbg ##
.TBG_queue="gpu"

.TBG_jobName="pic_tests"

# settings that can be controlled by environment variables before submit
.TBG_mailSettings=${MY_MAILNOTIFY:-"NONE"}
.TBG_mailAddress=${MY_MAIL:-"d.bertini@gsi.de"}
.TBG_author=${MY_NAME:+--author \"dbertini\"}
.TBG_nameProject=${PROJID:-"PIC"}
#.TBG_profile=${PIC_PROFILE:-"/lustre/rz/dbertini/gpu/picongpu.profile"}
.TBG_profile=${PIC_PROFILE:-"/lustre/rz/dbertini/gpu/vae_tests/picongpu.profile"}

# number of available/hosted devices per node in the system
.TBG_numHostedDevicesPerNode=8

# host memory per device
#.TBG_memPerDevice=64000000
.TBG_memPerDevice=28000000


# number of CPU cores to block per GPU
# we have 12 CPU cores per GPU (96cores/8gpus ~ 12cores)
#.TBG_coresPerGPU=16
.TBG_coresPerGPU=12

# Assign one OpenMP thread per available core per GPU (=task)
export OMP_NUM_THREADS=!TBG_coresPerGPU


# required GPUs per node for the current job
.TBG_devicesPerNode=$(if [ $TBG_tasks -gt $TBG_numHostedDevicesPerNode ] ; then echo $TBG_numHostedDevicesPerNode; else echo $TBG_tasks; fi)

# We only start 1 MPI task per device
.TBG_mpiTasksPerNode="$(( TBG_devicesPerNode * 1 ))"

# use ceil to caculate nodes
.TBG_nodes="$((( TBG_tasks + TBG_devicesPerNode - 1 ) / TBG_devicesPerNode))"

## end calculations ##

echo 'Running program...'

cd !TBG_dstPath

export MODULES_NO_OUTPUT=1

# Cannot call proflle without container
# source !TBG_profile
if [ $? -ne 0 ] ; then
  echo "Error: PIConGPU environment profile under \"!TBG_profile\" not found!"
  exit 1
fi
unset MODULES_NO_OUTPUT

# set user rights to u=rwx;g=r-x;o=---
umask 0027

mkdir simOutput 2> /dev/null
cd simOutput
ln -s ../stdout output

cat > !TBG_dstPath/tbg/pic_sub.sh <<EOF
#!/bin/bash
# Use pre-installed packages from VAE

echo " activate container profile"
. /vaesoft/thisvae.sh
.  !TBG_profile

!TBG_dstPath/input/bin/picongpu  !TBG_author !TBG_programParams

EOF
chmod +x !TBG_dstPath/tbg/pic_sub.sh 

export VAECONT=/cvmfs/vae.gsi.de/amdgpu/containers/user_container-develop.sif
#export SLURM_SINGULARITY_CONTAINER=/cvmfs/vae.gsi.de/amdgpu/containers/user_container-develop.sif

if [ $? -eq 0 ] ; then
  # Run PIConGPU from within the singularity container ?
   srun -K1 --export=ALL singularity exec  --bind /etc/slurm --bind /var/run/munge --bind /var/spool/slurm   --bind /var/lib/sss/pipes --bind /cvmfs --rocm $VAECONT  !TBG_dstPath/tbg/pic_sub.sh
   #srun -K1 --export=ALL  !TBG_dstPath/tbg/pic_sub.sh     
fi   
